#!/bin/sh

echo "Running migrations"
mix ecto.migrate

echo "Starting server... Access it in: http://$(hostname -i):$PORT/"
mix phx.server
