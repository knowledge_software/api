#!/bin/sh

set -e

# ELIXIR DEPS

mix deps.get
mix deps.compile --force

# NODE DEPS
if [[ ! -d ./assets/node_modules/brunch/ ]]; then
  echo "Install node dependencies in $(pwd)/assets"
  cd assets
  npm install
  cd ..
fi

if [[ -d ./assets/node_modules/brunch/bin/ ]]; then
  echo "Build js dependencies in $(pwd)/assets/node_modules/brunch/binb/brunch build assets"
  node ./assets/node_modules/brunch/bin/brunch build assets
  echo "Done"
fi

# DB DEPS
while ! nc -z $PG_HOST $PG_PORT;
do
  echo "$(date) - waiting for database $PG_HOST:$PG_PORT to start"
  sleep 3
done

mix ecto.create

# EXECUTE COMMAND
echo "Executing '$@'"
eval $@
