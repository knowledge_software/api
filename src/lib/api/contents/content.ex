defmodule Api.Contents.Content do
  use Ecto.Schema
  import Ecto.Changeset


  schema "contents" do
    field :abstract, :string
    field :body, :string
    field :is_finished, :boolean, default: false
    field :title, :string

    belongs_to :user, Api.Auth.User

    timestamps()
  end

  @doc false
  def changeset(content, attrs) do
    content
    |> cast(attrs, [:title, :abstract, :body, :is_finished, :user_id])
    |> validate_required([:title, :body, :is_finished, :user_id])
    |> validate_user_change()
  end

  @doc false
  def validate_user_change(%Ecto.Changeset{data: %Api.Contents.Content{user_id: nil}} = changes), do: changes
  def validate_user_change(%Ecto.Changeset{changes: %{user_id: user_id}} = changes) do
    add_error(changes, :user_id, "User is different from owner of content")
  end
  def validate_user_change(changes), do: changes
end
