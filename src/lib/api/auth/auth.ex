defmodule Api.Auth do
  @moduledoc """
  The Auth context.
  """

  import Ecto.Query, warn: false
  alias Api.Repo

  alias Api.Auth.User

  @doc """
  Authenticate a user
  Returns a token and clains
  ## Examples
      iex> user = %Api.Auth.User{email: "user@mail.org", password_digest: "FHIifi$fiow"}
      iex> authenticate(%{email: "user@mail.org", password: "mypassowrd"})
      {:ok, 'JWT token', %{}}

  """
  def find_and_authenticate(%{email: email, password: password}) do
    with {:ok, %User{} = user} <- find_by(%{email: email}) do
      case Comeonin.Argon2.checkpw(password, user.password_digest) do
        true -> Api.Auth.Guardian.encode_and_sign(user)
        _ -> {:error, :unauthorized}
      end
    end
  end
  @doc """
  Gets a single user based in email to authentication.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def find_by(%{email: email}) do
    case Repo.get_by(User, email: email) do
      nil -> 
        {:error, :unauthorized}
      user ->
        {:ok, user}
    end
  end

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end
end
