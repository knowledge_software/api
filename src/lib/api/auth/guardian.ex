defmodule Api.Auth.Guardian do
  use Guardian, otp_app: :api
  alias Api.Auth

  @doc """
  Define the subject to be added in token
  """
  def subject_for_token(user, _claims) do
    sub = to_string(user.id)
    {:ok, sub}
  end

  def subject_for_token(_, _) do
    {:error, :reason_for_error}
  end

  @doc """
  Recover the subject from token
  """
  def resource_from_claims(claims) do
    id = claims["sub"]
    user = Auth.get_user!(id)
    {:ok,  user}
  end

  def resource_from_claims(_claims) do
    {:error, :reason_for_error}
  end
end
