defmodule Api.Auth.User do
  use Ecto.Schema
  import Ecto.Changeset

  alias Comeonin.Argon2


  schema "users" do
    field :email, :string
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true
    field :password_digest, :string
    field :username, :string

    has_many :contents, Api.Contents.Content

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :password_digest, :password, :password_confirmation, :email])
    |> validate_required([:username, :password, :password_confirmation, :email])
    |> validate_confirmation(:password)
    |> put_password()
    |> change(%{password:  nil, password_confirmation: nil})
  end

  @doc """
  It add the hash into password of user
  """
  defp put_password(%Ecto.Changeset{valid?: true, changes: %{password: password}} = changeset) do
    put_change(changeset, :password_digest, Argon2.hashpwsalt(password))
  end

  @doc """
  It just return the user without changes in password user
  """
  defp put_password(changeset) do
    changeset
  end

end
