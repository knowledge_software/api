defmodule ApiWeb.ContentView do
  use ApiWeb, :view
  alias ApiWeb.ContentView

  def render("index.json", %{contents: contents}) do
    %{data: render_many(contents, ContentView, "content.json")}
  end

  def render("show.json", %{content: content}) do
    %{data: render_one(content, ContentView, "content.json")}
  end

  def render("content.json", %{content: content}) do
    %{id: content.id,
      title: content.title,
      abstract: content.abstract,
      body: content.body,
      is_finished: content.is_finished,
      user_id: content.user_id}
  end
end
