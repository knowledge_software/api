defmodule ApiWeb.SessionView do
  use ApiWeb, :view

  def render("token.json", %{token: token}) do
    %{token: token,
      message: "Login in successful"}
  end
end
