defmodule ApiWeb.AuthorizationView do
  use ApiWeb, :view

  @doc """
  Show a json with error of unauthorized
  """
  def render("error.json", %{status: status, message: message}) do
    %{status: status, message: message}
  end

end
