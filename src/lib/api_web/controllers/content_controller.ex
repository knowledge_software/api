defmodule ApiWeb.ContentController do
  use ApiWeb, :controller


  alias Api.Contents
  alias Api.Contents.Content

  action_fallback ApiWeb.FallbackController

  def index(conn, _params) do
    contents = Contents.list_contents()
    render(conn, "index.json", contents: contents)
  end

  def create(conn, %{"content" => content_params}) do
    user_id = Guardian.Plug.current_resource(conn).id |> to_string
    content_params = content_params |> Map.put("user_id", user_id)
    with {:ok, %Content{} = content} <- Contents.create_content(content_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", content_path(conn, :show, content))
      |> render("show.json", content: content)
    end
  end

  def show(conn, %{"id" => id}) do
    content = Contents.get_content!(id)
    render(conn, "show.json", content: content)
  end

  def update(conn, %{"id" => id, "content" => content_params}) do
    content = Contents.get_content!(id)
    with {:ok, %Content{} = content} <- Contents.update_content(content, content_params) do
      render(conn, "show.json", content: content)
    end
  end

  def delete(conn, %{"id" => id}) do
    content = Contents.get_content!(id)
    with {:ok, %Content{}} <- Contents.delete_content(content) do
      send_resp(conn, :no_content, "")
    end
  end
end
