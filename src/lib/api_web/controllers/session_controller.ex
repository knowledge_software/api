defmodule ApiWeb.SessionController do
  use ApiWeb, :controller

  action_fallback ApiWeb.FallbackAPIController

  def sign_in(conn, %{"email" => email, "password" => password} = params) do
    new_params = map_to_atom(params)
    with {:ok, token, _claims} <- Api.Auth.find_and_authenticate(new_params) do
      # Render the token
      render(conn, "token.json", token: token)
    end
  end

  defp map_to_atom(%{} = params) do
    for {key, value} <- params, into: %{}, do: {String.to_atom(key), value}
  end

end
