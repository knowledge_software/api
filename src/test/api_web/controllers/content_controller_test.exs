defmodule ApiWeb.ContentControllerTest do
  use ApiWeb.ConnCase

  alias Api.Contents
  alias Api.Contents.Content

  @create_attrs %{abstract: "some abstract", body: "some body", is_finished: true, title: "some title"}
  @update_attrs %{abstract: "some updated abstract", body: "some updated body", is_finished: false, title: "some updated title"}
  @invalid_attrs %{abstract: nil, body: nil, is_finished: nil, title: nil}

  def fixture(:content) do
    {:ok, content} = Contents.create_content(@create_attrs)
    content
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all contents", %{conn: conn} do
      conn = get conn, content_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create content" do
    test "renders content when data is valid", %{conn: conn} do
      conn = post conn, content_path(conn, :create), content: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, content_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "abstract" => "some abstract",
        "body" => "some body",
        "is_finished" => true,
        "title" => "some title"}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, content_path(conn, :create), content: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update content" do
    setup [:create_content]

    test "renders content when data is valid", %{conn: conn, content: %Content{id: id} = content} do
      conn = put conn, content_path(conn, :update, content), content: @update_attrs
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get conn, content_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "abstract" => "some updated abstract",
        "body" => "some updated body",
        "is_finished" => false,
        "title" => "some updated title"}
    end

    test "renders errors when data is invalid", %{conn: conn, content: content} do
      conn = put conn, content_path(conn, :update, content), content: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete content" do
    setup [:create_content]

    test "deletes chosen content", %{conn: conn, content: content} do
      conn = delete conn, content_path(conn, :delete, content)
      assert response(conn, 204)
      assert_error_sent 404, fn ->
        get conn, content_path(conn, :show, content)
      end
    end
  end

  defp create_content(_) do
    content = fixture(:content)
    {:ok, content: content}
  end
end
