defmodule Api.AuthTest do
  use Api.DataCase

  alias Api.Auth
  alias Comeonin.Argon2


  describe "authentication" do

    @valid_attrs %{email: "some email", password: "some password", password_confirmation: "some password", username: "some username"}
    @valid_credentials %{email: "some email", password: "some password"}
    @invalid_credentials %{email: "some email", password: "s"}
    @valid_email %{email: "some email"}
    @invalid_email %{email: "email some"}

    def user_fixture() do
      {:ok, user} =
        Enum.into(%{}, @valid_attrs)
        |> Auth.create_user()
      user
    end

    test "find_by/1 return a user" do
      user = user_fixture()
      assert {:ok, user} == Auth.find_by(@valid_email)
    end

    test "find_by/1 return unauthorized" do
      assert {:error, :unauthorized} = Auth.find_by(@invalid_email)
    end

    test "find_and_authenticate/1 error" do
      user = user_fixture()
      assert {:error, :unauthorized} = Auth.find_and_authenticate(@invalid_credentials)
    end

  end

  describe "users" do
    alias Api.Auth.User

    @valid_attrs %{email: "some email", password: "some password", password_confirmation: "some password", username: "some username"}
    @update_attrs %{email: "some updated email", password: "some updated password", password_confirmation: "some updated password", username: "some updated username"}
    @invalid_attrs %{email: nil, password: nil, password_confirmation: nil, username: nil}
    @invalid_passwords %{email: "some email", password: "some password", password_confirmation: "some different password", username: "my username"}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Auth.create_user()
      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Auth.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Auth.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Auth.create_user(@valid_attrs)
      assert user.email == "some email"
      assert user.password == nil
      assert user.password_confirmation == nil
      assert Argon2.checkpw("some password", user.password_digest)
      assert user.username == "some username"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Auth.create_user(@invalid_attrs)
    end

    test "create_user/1 with invalid passwords data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Auth.create_user(@invalid_passwords)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, user} = Auth.update_user(user, @update_attrs)
      assert %User{} = user
      assert user.email == "some updated email"
      assert user.password == nil
      assert user.password_confirmation == nil
      assert Argon2.checkpw("some updated password", user.password_digest)
      assert user.username == "some updated username"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Auth.update_user(user, @invalid_attrs)
      assert user == Auth.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Auth.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Auth.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Auth.change_user(user)
    end
  end
end
