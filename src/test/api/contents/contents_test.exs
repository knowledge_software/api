defmodule Api.ContentsTest do
  use Api.DataCase

  alias Api.Contents

  describe "contents" do
    alias Api.Contents.Content

    @valid_attrs %{abstract: "some abstract", body: "some body", is_finished: true, title: "some title"}
    @update_attrs %{abstract: "some updated abstract", body: "some updated body", is_finished: false, title: "some updated title"}
    @invalid_attrs %{abstract: nil, body: nil, is_finished: nil, title: nil}

    def content_fixture(attrs \\ %{}) do
      {:ok, content} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Contents.create_content()

      content
    end

    test "list_contents/0 returns all contents" do
      content = content_fixture()
      assert Contents.list_contents() == [content]
    end

    test "get_content!/1 returns the content with given id" do
      content = content_fixture()
      assert Contents.get_content!(content.id) == content
    end

    test "create_content/1 with valid data creates a content" do
      assert {:ok, %Content{} = content} = Contents.create_content(@valid_attrs)
      assert content.abstract == "some abstract"
      assert content.body == "some body"
      assert content.is_finished == true
      assert content.title == "some title"
    end

    test "create_content/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Contents.create_content(@invalid_attrs)
    end

    test "update_content/2 with valid data updates the content" do
      content = content_fixture()
      assert {:ok, content} = Contents.update_content(content, @update_attrs)
      assert %Content{} = content
      assert content.abstract == "some updated abstract"
      assert content.body == "some updated body"
      assert content.is_finished == false
      assert content.title == "some updated title"
    end

    test "update_content/2 with invalid data returns error changeset" do
      content = content_fixture()
      assert {:error, %Ecto.Changeset{}} = Contents.update_content(content, @invalid_attrs)
      assert content == Contents.get_content!(content.id)
    end

    test "delete_content/1 deletes the content" do
      content = content_fixture()
      assert {:ok, %Content{}} = Contents.delete_content(content)
      assert_raise Ecto.NoResultsError, fn -> Contents.get_content!(content.id) end
    end

    test "change_content/1 returns a content changeset" do
      content = content_fixture()
      assert %Ecto.Changeset{} = Contents.change_content(content)
    end
  end
end
