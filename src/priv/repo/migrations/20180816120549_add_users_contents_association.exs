defmodule Api.Repo.Migrations.AddUsersContentsAssociation do
  use Ecto.Migration

  def change do
    alter table(:contents) do
      add :user_id, references(:users), on_delete: :delete_all
    end
  end
end
