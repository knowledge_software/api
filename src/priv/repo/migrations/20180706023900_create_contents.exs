defmodule Api.Repo.Migrations.CreateContents do
  use Ecto.Migration

  def change do
    create table(:contents) do
      add :title, :string
      add :abstract, :string
      add :body, :string
      add :is_finished, :boolean, default: false, null: false

      timestamps()
    end

  end
end
