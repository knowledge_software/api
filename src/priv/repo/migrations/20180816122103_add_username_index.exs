defmodule Api.Repo.Migrations.AddUsernameIndex do
  use Ecto.Migration

  def change do
    create index("users", [:username], unique: true)
  end
end
