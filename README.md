# Using API

## CURL

### Create a user

```
$ curl -H "Accept: application/json" \
  -H "Content-Type: application/json" \
  -X POST \
  -d '{"user": {"email": "marcelohpf@gmail.com", "password": "1234", "password_confirmation": "1234", "username": "marcelohpf"}}' \
  http://localhost:4000/api/users/
```  
```
{"data":{"username":"marcelohpf","password_confirmation":null,"password":null,"id":0,"email":"marcelohpf@gmail.com"}}
```

### Sign in

```
$ curl -H "Accept: application/json" \
  -H "Content-Type: application/json" \
  -X POST \
  -d '{"email": "marcelohpf@gmail.com", "password": "1234"}' \
  http://localhost:4000/api/sign_in/
```
```
{"token":"eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJhcGkiLCJleHAiOjE1MzMyNjU1ODMsImlhdCI6MTUzMDg0NjM4MywiaXNzIjoiYXBpIiwianRpIjoiNWRiY2FiN2UtZmNjZC00YmI3LWI5ODAtZTgwYTI3MTE5NGI3IiwibmJmIjoxNTMwODQ2MzgyLCJzdWIiOiIxIiwidHlwIjoiYWNjZXNzIn0.RhW1tsv6cyUc51HmQECiLL59lrNoR2NUrkp-WzRfn7HpRyVxlV2jVe1CbCUL0sEC1V2CQEuRRq3W-9rzLcQXtA","message":"Login in successful"}
```

### Create a content

```
$ curl \
  -H "Accept: application/json" \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer <token>" \
  -X POST \
  -d '{"content": {"title": "title 1", "abstract": "simple abstract if necessary", "body": "This is a simple demonstration body to a request using the command line", "user_id": "1"}}' \
  http://localhost:4000/api/contents/
```
```
{"data":{"title":"title 1","is_finished":false,"id":2,"body":"This is a simple demonstration body to a request using the command line","abstract":"simple abstract if necessary"}}
```

### Update a content

```
$ curl \
  -H "Accept: application/json" \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer <token>" \
  -X PUT \
  -d '{"content": {"title": "t", "abstract": "a", "body": "b", "user_id": "1"}}' \
  http://localhost:4000/api/contents/<id>/
```